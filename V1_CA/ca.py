# CA Simulation
#
#        Author: Sida Liu, Connor Klopfer, Wyatt Wu
#        Date: 2019-09-25
#
# Here we propose a way of simulating the dynamics of bacteria, white blood cell and drugs.
#
# Cell States:
#   00 Empty
#   01 White blood cell (WBC)
#   10 Bacteria Level 0 (B0): can be killed by WBCs 
#   11 Bacteria Level 1 (B1): cannot be killed by WBCs
#   12 Bacteria Level 2 (B2): cannote be killed by WBCs and can protect its Moore neighbors from be killed by WBCs
#
# Initial State:
#   One B0 in the middle, other cells are all empty.
#
# Rules at each step:
#   1. a B0 has limited life span, after several steps, it will die naturally.
#   2. if a B0 has any empty Moore neighbor cells, it will turn the empty neighbor cell into B0. 
#
# Parameters:
#   1. life span of B0

from CAModel import CAModel
from CAView import CAView

if __name__ == "__main__":
    model = CAModel()
    view = CAView(model=model)
    view.show()
