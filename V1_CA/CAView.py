import matplotlib. pyplot as plt
from matplotlib import animation, colors
import numpy as np
import time
#
# CAView(MVC -- View part)
#   Dealing with the Graphical User Interface, such as layout, color, animation, etc.
#   Typical usage:
#       view = CAView()
#       view.set_data_feed(data_feed_func)
#       view.show()
#
class CAView:
    def __init__(self, model):
        self.model = model
        self.state_index = np.arange(len(model.Cell_State)+1)
        self.colorbrew2 = ['#8dd3c7', '#ffffb3', '#bebada', '#fb8072']#, '#80b1d3', '#fdb462', '#b3de69']
        self.colorbrew2 = ['#AAFFCC', '#FFFFFF', 'orange', 'red', 'purple']
        self.cmap = colors.ListedColormap(self.colorbrew2)
        self.color_bounds = self.state_index-0.5
        self.color_norm = colors.BoundaryNorm(self.color_bounds, self.cmap.N)
        self.layout()

    def layout(self):
        self.fig = plt.figure(figsize=(16, 14))
        self.fig.canvas.set_window_title('CA Simulation - Bectaria v.s. White Blood Cells')

        grid = plt.GridSpec(3, 4, hspace=0.4, wspace=0.2)
        self.main_ax = self.fig.add_subplot(grid[:, 1:])
        self.main_ax.get_xaxis().set_ticks([])
        self.main_ax.get_yaxis().set_ticks([])
        self.info_ax = []
        for i in range(3):
            self.info_ax.append(self.fig.add_subplot(grid[i, 0]))
        self.info_ax[0].set_title("Population Statistics")
        self.info_ax[0].set_ylim([0,1])
        self.info_ax[1].set_title("Population Derivatives")
        self.info_ax[2].axis('off')
        self.info_ax[2].plot([1],[1], c=self.colorbrew2[0], label="Empty")
        self.info_ax[2].plot([1],[1], c=self.colorbrew2[1], label="WBC")
        self.info_ax[2].plot([1],[1], c=self.colorbrew2[2], label="Bacteria Level 0")
        self.info_ax[2].plot([1],[1], c=self.colorbrew2[3], label="Bacteria Level 1")
        self.info_ax[2].plot([1],[1], c=self.colorbrew2[4], label="Bacteria Level 2")
        self.info_ax[2].legend(loc='lower left')
        #img = self.info_ax[2].imshow([[1]], cmap=self.cmap, norm=self.color_norm)
        #plt.colorbar(img, ax=self.info_ax[2], orientation='horizontal', cmap=self.cmap, norm=self.color_norm, boundaries=self.color_bounds, ticks=self.state_index)

        self.main_image = self.main_ax.imshow(self.model.main_data, cmap=self.cmap, norm=self.color_norm)

        self.population_statistics = []
        for i in range(len(self.model.count_types)):
            line,  = self.info_ax[0].plot([],[], color=self.colorbrew2[i+2]) # need a better way to align colors
            self.population_statistics.append(line)

        self.population_derivative = []
        for i in range(len(self.model.count_types)):
            line,  = self.info_ax[1].plot([],[], color=self.colorbrew2[i+2]) # need a better way to align colors
            self.population_derivative.append(line)

    def frames(self):
        while True:
            yield self.model.data_generator()

    def animate(self, args):
        #time.sleep(0.3)
        print(self.model.step)
        self.main_image.set_data(self.model.main_data)
        
        self.info_ax[0].set_xlim([0, self.model.population_statistics.shape[0]])
        self.info_ax[0].set_ylim([0, np.max(self.model.population_statistics[:,1:])/self.model.CA_world_total_cell])
        self.info_ax[1].set_xlim([0, self.model.population_statistics.shape[0]])
        self.info_ax[1].set_ylim([np.min(self.model.population_derivative[:,1:]), 1+np.max(self.model.population_derivative[:,1:])])

        for i in range(self.model.population_statistics.shape[1]-1):
            x = self.model.population_statistics[:,0]
            y = self.model.population_statistics[:,i+1] / self.model.CA_world_total_cell
            self.population_statistics[i].set_data(x,y)
            y = self.model.population_derivative[:,i+1] 
            self.population_derivative[i].set_data(x,y)

    def show(self):
        # Notice: must receive and save the return of FuncAnimation
        # otherwise the animation will free itself and never work.
        self.animation = animation.FuncAnimation(self.fig, self.animate, frames=self.frames, save_count=200)
        plt.show()
        # Make a video, Faster while doing presentation.
        #self.animation.save(filename='./animation.mp4')


if __name__ == "__main__":
    from CAModel import CAModel
    model = CAModel()
    view = CAView(model=model)
    view.show()
