import numpy as np
# Cell States:
#   00 Empty
#   01 White blood cell (WBC)
#   10 Bacteria Level 0 (B0): can be killed by WBCs 
#   11 Bacteria Level 1 (B1): cannot be killed by WBCs
#   12 Bacteria Level 2 (B2): cannote be killed by WBCs and can protect its Moore neighbors from be killed by WBCs

#
# CAModel(MVC -- Model part)
#   Dealing with the system data and logic.
#   Computational result at each step will be provide via data() function
class CAModel:
    def __init__(self):
        self.CA_world_size = (200,200)
        self.CA_world_total_cell = self.CA_world_size[0] * self.CA_world_size[1]
        self.Cell_State = {
            "Empty": 0,
            "WBC": 1,
            "B0": 2,
            "B1": 3,
            "B2": 4
        }
        self.count_types = ["B0", "B1", "B2"]

        self.main_data = np.zeros(self.CA_world_size, dtype='i')
        # save 10 steps in order to check for life span
        self.main_data_history = [self.main_data.copy()] * 10
        
        self.step = 0

        self.population_statistics = None
        self.population_derivative = None

        # example intial bacteria
        self.main_data[int(self.CA_world_size[0]/2), int(self.CA_world_size[1]/2)] = self.Cell_State["B0"]
    
    def Rule_life_span(self, new_data):
        # --Rule 1: life span
        # Rule logic express by numpy array
        # select any cell with "thing"s. selected will be an array with True and False.
        selected = self.main_data>0
        # if previous 10 steps are same with current step, means that "thing" lived for at least 10 steps.
        selected = np.logical_and.reduce( (
            selected,
            (self.main_data_history[0] == self.main_data),
            (self.main_data_history[1] == self.main_data),
            (self.main_data_history[2] == self.main_data),
            (self.main_data_history[3] == self.main_data),
            (self.main_data_history[4] == self.main_data),
            (self.main_data_history[5] == self.main_data),
            (self.main_data_history[6] == self.main_data),
            (self.main_data_history[7] == self.main_data),
            (self.main_data_history[8] == self.main_data),
            (self.main_data_history[9] == self.main_data)
        ))
        # set all those old "thing"s to die
        new_data[selected] = self.Cell_State["Empty"]
        return new_data

    def Rule_bacteria_growth(self, new_data):
        # --Rule: bacteria give birth to the cell next to it
        # Rule logic express by numpy array
        parent_types = ["B0", "B1", "B2"]
        # grow in random order, so no species can have absolute advantage.
        random_orders = np.arange(3)
        np.random.shuffle(random_orders)
        # every step bacteria has a probability of 0.1 to grow?
        random_growth = np.random.rand(self.main_data.shape[0], self.main_data.shape[1])<0.2
        random_mutation = np.random.rand(self.main_data.shape[0], self.main_data.shape[1])<0.001
        # populate all bacteria types
        for random_order in random_orders:
            parent_type = parent_types[random_order]
            # select all empty cells
            empty_cell = self.main_data==0
            # check Von Neumann neighborhood
            b_0 = np.roll(self.main_data, 1, axis=0)==self.Cell_State[parent_type]
            b_1 = np.roll(self.main_data, -1, axis=0)==self.Cell_State[parent_type]
            b_2 = np.roll(self.main_data, 1, axis=1)==self.Cell_State[parent_type]
            b_3 = np.roll(self.main_data, -1, axis=1)==self.Cell_State[parent_type]
            # apply logic, np.logical_or.reduce is basically means do logical_or to all b0, b1, etc.
            has_neighbor = np.logical_or.reduce( (b_0,b_1,b_2,b_3) )
            selected = np.logical_and.reduce( (random_growth, empty_cell, has_neighbor) )
            # set all empty cell who has at least one neighbor to it's neighbor
            new_data[selected] = self.Cell_State[parent_type]
            # mutate to +1 level
            if (parent_type != "B2"):
                selected = np.logical_and.reduce( (selected, random_mutation) )
                new_data[selected] = self.Cell_State[parent_type] + 1
        return new_data

    def Rule_wbc_occurs(self, new_data):
        # --Rule: WBC occurs in empty cells due to random chance
        empty_cell = self.main_data==0
        probability_of_wbc = np.sum(self.main_data>=self.Cell_State["B0"]) / self.CA_world_total_cell
        print(probability_of_wbc)
        random_chance = np.random.rand(self.main_data.shape[0], self.main_data.shape[1])<probability_of_wbc
        selected = np.logical_and.reduce( (empty_cell, random_chance) )
        new_data[selected] = self.Cell_State["WBC"]
        return new_data

    def Rule_wbc_kill_b0(self, new_data):
        # --Rule: WBC kills it's neighbor bacteria, but B2 protect its neighbor, B1 cannot be killed.
        # on the other words, B0 without B2 as its neighbor and with WBC as its neighbor die.
        B0_cell = self.main_data==self.Cell_State["B0"]
        # check Von Neumann neighborhood
        wbc_0 = np.roll(self.main_data, 1, axis=0)==self.Cell_State["WBC"]
        wbc_1 = np.roll(self.main_data, -1, axis=0)==self.Cell_State["WBC"]
        wbc_2 = np.roll(self.main_data, 1, axis=1)==self.Cell_State["WBC"]
        wbc_3 = np.roll(self.main_data, -1, axis=1)==self.Cell_State["WBC"]
        b2_0 = np.roll(self.main_data, 1, axis=0)!=self.Cell_State["B2"]
        b2_1 = np.roll(self.main_data, -1, axis=0)!=self.Cell_State["B2"]
        b2_2 = np.roll(self.main_data, 1, axis=1)!=self.Cell_State["B2"]
        b2_3 = np.roll(self.main_data, -1, axis=1)!=self.Cell_State["B2"]
        selected = np.logical_or.reduce( (wbc_0, wbc_1, wbc_2, wbc_3) )
        selected = np.logical_and.reduce( (selected, B0_cell, b2_0, b2_1, b2_2, b2_3) )
        new_data[selected] = self.Cell_State["Empty"]
        return new_data

    def data_generator(self):
        # Just some temperary example rules here. numpy operation should be considered, to make things faster.

        # Step 1. Create memory space for new data
        new_data = self.main_data.copy()
        # Step 2. Modify new data based on rules of each step. (We should avoid for-loop here as much as possible.)
        new_data = self.Rule_life_span(new_data)
        new_data = self.Rule_bacteria_growth(new_data)
        new_data = self.Rule_wbc_occurs(new_data)
        new_data = self.Rule_wbc_kill_b0(new_data)
       
        # Step 3. Save current main data to history and update current main data
        for i in range(9,0,-1):
            self.main_data_history[i] = self.main_data_history[i-1]
        self.main_data_history[0] = self.main_data.copy()
        self.main_data = new_data

        # Step 4. Save population statistics
        ret = np.zeros(len(self.count_types)+1, dtype='i')
        ret[0] = self.step
        for index, count_type in enumerate(self.count_types):
            count = (self.main_data==self.Cell_State[count_type]).sum()
            ret[index+1] = count
        ret = ret.reshape(1,-1)
        if self.step>0:
            self.population_statistics = np.append( self.population_statistics, ret, axis=0 )
            self.population_derivative = np.append( self.population_derivative, ret-self.population_statistics[-2], axis=0 )
        else:
            self.population_statistics = ret
            self.population_derivative = np.zeros_like(ret, dtype='i')
        self.step += 1


if __name__ == "__main__":
    from CAView import CAView
    model = CAModel()
    view = CAView(model=model)
    view.show()
