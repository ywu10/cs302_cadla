import numpy as np
import matplotlib.pyplot as plt

space_size = 5
space = np.zeros((space_size, space_size), dtype='b')

particles = np.random.rand(space_size,space_size)<0.1

#print(particles)

x = np.cumsum((np.random.rand(100,10000,2)<0.5)*2-1, axis=1)

print(x.shape)
for i in range(10):
    plt.plot(x[i,:,0],x[i,:,1])
plt.show()