import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import time, sys
import pickle
from numba import jit
import scipy.ndimage

dla_world_size = 1000

def init():
    global dla_main_data, graph_radius,c 
    dla_main_data = np.zeros((dla_world_size,dla_world_size), dtype='i')
    dla_main_data[int(dla_world_size/2), int(dla_world_size/2)] = 1
    graph_radius = 1
    c = 0
init()


fig = plt.figure(figsize=[16,16])
main_image = plt.imshow(dla_main_data.T, cmap='terrain_r', origin='lower')

@jit(nopython=True)
def fast_compute(c, graph_radius, dla_main_data, probable_fire_site):
    ret_c = c
    ret_graph_radius = graph_radius
    ret_dla_main_data = dla_main_data

    if c%100==0:
        print("legal particle ", c, "; graph_radius ", graph_radius, "; ")

    end = 0
    while not end:
        # generate a new particle around the graph
        xy = np.nonzero(probable_fire_site)
        r = np.random.randint(0,len(xy[0]))
        x = xy[0][r]
        y = xy[1][r]

        for j in range(2000): #<- max step of random walk
            #dla_main_data[x,y] = 2
            # if touch boudary, abandon this particle
            if x<=1 or y<=1 or x>=dla_world_size-1 or y>=dla_world_size-1:
                break
            # if touch graph, grow
            if dla_main_data[x-1,y]==1 or dla_main_data[x,y-1]==1 or dla_main_data[x+1,y]==1 or dla_main_data[x,y+1]==1:
                ret_dla_main_data[x,y] = 1
                distance = np.linalg.norm(np.array([x-dla_world_size/2, y-dla_world_size/2]))
                if distance>graph_radius:
                    ret_graph_radius = distance
                ret_c += 1
                end = 1
                break
            # random walk
            r = np.random.randint(0,4)
            if r==1:
                x-=1
            elif r==2:
                y-=1
            elif r==3:
                x+=1
            else:
                y+=1
    return ret_c, ret_graph_radius, ret_dla_main_data

def animate(args):
    global c, graph_radius, dla_main_data
    p1 = scipy.ndimage.binary_dilation(dla_main_data, iterations=5).astype(dla_main_data.dtype)
    p2 = scipy.ndimage.binary_dilation(p1, iterations=5).astype(dla_main_data.dtype)
    probable_fire_site = np.logical_and(p2, p1==0).astype(dla_main_data.dtype)
    c, graph_radius, dla_main_data = fast_compute( c, graph_radius, dla_main_data, probable_fire_site )
    main_image.set_data(dla_main_data.T)

timestamp = time.strftime("%Y%m%d_%H%M%S")
if len(sys.argv)>1:
    total_particles = int(sys.argv[1])
else:
    total_particles = 300
if True: #<- animation
    ani = animation.FuncAnimation(fig, animate, save_count=total_particles-1)
    # if there's an arg in command line, like `python dla.py v`, then produce the movie. otherwise, show animation.
    if len(sys.argv)>1:
        ani.save("data/dla_main_data_%d_particles_%s.mp4"%(total_particles, timestamp), fps=50)
    else:
        plt.show()
else: #<- only calculation
    init()
    for i in range(total_particles-2):
        animate(0)
plt.imshow(dla_main_data.T, cmap='terrain_r', origin='lower')
plt.savefig('data/dla_main_data_%d_particles_%s.png'%(total_particles, timestamp))
f = open('data/dla_main_data_%d_particles_%s.pickle'%(total_particles, timestamp), "wb")
pickle.dump(dla_main_data, f)
f.close()
