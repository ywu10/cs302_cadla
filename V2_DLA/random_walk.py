import numpy as np
import matplotlib.pyplot as plt
from numba import jit

@jit(nopython=True)
def random_walk(num_experiment=10000, total_steps=10000):
    result = []
    for experiment in range(num_experiment):
        if experiment%100==0:
            print("experiment ", experiment)
        x,y = 0,0
        for step in range(total_steps):
            rand = np.random.randint(0,4)
            if rand==0:
                x -= 1
            elif rand==1:
                y -= 1
            elif rand==2:
                x += 1
            elif rand==3:
                y += 1
        result.append(np.sqrt(x**2+y**2))
    return result

result = random_walk()
plt.hist(result, bins=40)
plt.show()
#print(result)
exit()

step_size = 3
step_direction = 0

step_direction = np.random.rand(1) * 2 * np.pi

shift_x = np.sin(step_direction) * step_size
shift_y = np.cos(step_direction) * step_size

#print(shift_x, shift_y)

@jit(nopython=True)
def fast_compute():
    return 0

#fast_compute()