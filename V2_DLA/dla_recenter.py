import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import time, sys
import pickle
from numba import jit
import scipy.ndimage

dla_world_size = 600

def init():
    global dla_main_data, graph_radius,c 
    dla_main_data = np.zeros((dla_world_size,dla_world_size), dtype='i')
    dla_main_data[int(dla_world_size/2), int(dla_world_size/2)] = 1
    graph_radius = 1
    c = 0
init()


fig = plt.figure(figsize=[16,16])
main_image = plt.imshow(dla_main_data.T, cmap='terrain_r', origin='lower')

# center_of_mass, comes from scipy.ndimage.center_of_mass, just modified to be support by numba
@jit(nopython=True)
def center_of_mass(input):
    grids = []
    grids.append(np.arange(input.shape[0]).reshape(-1,1))
    grids.append(np.arange(input.shape[1]).reshape(1,-1))
    normalizer = np.sum(input)
    s_i = np.sum( input*grids[0] )/normalizer
    s_j = np.sum( input*grids[1] )/normalizer
    return s_i, s_j


#@jit(nopython=True)
def fast_compute(c, graph_radius, dla_main_data):
    ret_c = c
    ret_graph_radius = graph_radius
    ret_dla_main_data = dla_main_data

    if c%100==0:
        print("legal particle ", c, "; graph_radius ", graph_radius, "; ")

    end = 0
    while not end:
        graph_center_x, graph_center_y = center_of_mass(dla_main_data) #<- recenter to the new center of graph
        # generate a new particle around the graph
        theta = np.random.rand()
        radius = 1 * graph_radius + 10 #<- new particle will be 1.1 * graph_radius + 10 away from the origin. graph_radius will be updated dynamically.
        x = int(radius * np.sin(theta*2*np.pi)+graph_center_x)
        y = int(radius * np.cos(theta*2*np.pi)+graph_center_y)

        for j in range(2000): #<- max step of random walk
            #dla_main_data[x,y] = 2
            # if touch boudary, abandon this particle
            if x<=1 or y<=1 or x>=dla_world_size-1 or y>=dla_world_size-1:
                break
            # if touch graph, grow
            if dla_main_data[x-1,y]==1 or dla_main_data[x,y-1]==1 or dla_main_data[x+1,y]==1 or dla_main_data[x,y+1]==1:
                ret_dla_main_data[x,y] = 1
                distance = np.linalg.norm(np.array([x-dla_world_size/2, y-dla_world_size/2]))
                if distance>graph_radius:
                    ret_graph_radius = distance
                ret_c += 1
                end = 1
                break
            # random walk
            r = np.random.randint(0,4)
            if r==1:
                x-=1
            elif r==2:
                y-=1
            elif r==3:
                x+=1
            else:
                y+=1
    return ret_c, ret_graph_radius, ret_dla_main_data

def animate(args):
    global c, graph_radius, dla_main_data
    c, graph_radius, dla_main_data = fast_compute( c, graph_radius, dla_main_data )
    main_image.set_data(dla_main_data.T)

if True: #<- animation
    ani = animation.FuncAnimation(fig, animate, save_count=3000)
    # if there's an arg in command line, like `python dla.py v`, then produce the movie. otherwise, show animation.
    if len(sys.argv)>1:
        ani.save("DLA_%s.mp4"%(time.strftime("%Y%m%d_%H%M%S")), fps=50)
    else:
        plt.show()
else: #<- only calculation
    a = time.time()
    for j in range(10): #<- do the experiment 10 times, generate 10 pieces of data
        init()
        print(j,"round")
        for i in range(3000):
            animate(0)
        plt.imshow(dla_main_data.T, cmap='terrain_r', origin='lower')
        plt.show()
        #print(dla_main_data.shape)
        f = open("data/dla_main_data_%d_particles_%s.pickle"%(i+2, time.strftime("%Y%m%d_%H%M%S")), "wb")
        pickle.dump(dla_main_data, f)
        f.close()
    b = time.time()
    print("total time: ", b-a)