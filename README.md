# CS302: CA and DLA

Cellular Automata and Diffusion-Limited Aggregation

## Use pipenv to install dependencies

```bash
~/cs302_cadla/$ pip install pipenv
~/cs302_cadla/$ pipenv install
~/cs302_cadla/$ pipenv run python V2_DLA/dla.py
```

So we use `pipenv run python V2_DLA/dla.py` instead of `python dla.py`.

## Making video using FFMpeg

please download ffmpeg.exe from https://ffmpeg.zeranoe.com/builds/ and place the file in %PATH%.

## Run

### 1. CA model

```python
python V1_CA/ca.py
```

### 2. DLA model

```python
python V2_DLA/dla.py
```

## Fast computation using Numba

Please install Numba. In the program, there's some computational intensive function, which can be compiled to machine code using the @jit(nopython=True) decoration, and the program will run way faster than only Python.

## maintainor

Connor L Klopfer (cklopfer@uvm.edu)

Sida Liu (sliu1@uvm.edu)

Wyatt Wu (ywu10@uvm.edu)
